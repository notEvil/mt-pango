def pytest_addoption(parser):
    parser.addoption(
        "--show",
        action="store_true",
        default=False,
        help="Shows the results in GTK windows.",
    )
