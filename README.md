# mt_pango

mt_pango is a [Markdown](https://markdown.de/) to [Pango](https://pango.gnome.org/) renderer based on [mistletoe](https://github.com/miyuchina/mistletoe). I started this as a side project to make [xdot](https://github.com/jrfonseca/xdot.py)'s tooltips better ([#93](https://github.com/jrfonseca/xdot.py/issues/93)) and it is currently a little more than a proof of concept: working but I wouldn't put my money on it!

I will put it on PyPI when requested/required.

### Quickstart

The easiest way to get this working is with pip and build.

```bash
git clone "https://gitlab.com/notEvil/mt-pango"
cd "./mt-pango"
pip install build
python -m build
pip install "./dist/mt_pango-0.0.1.tar.gz"[test]
pytest --show
```

Also check out [pipenv](https://github.com/pypa/pipenv) if you didn't already!
