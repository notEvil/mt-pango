from gi.repository import GLib
import mistletoe
import mistletoe.base_renderer as m_base_renderer
import math


def unknown(function):
    def _unknown(*args, **kwargs):
        arguments = [repr(argument) for argument in args]
        arguments.extend(
            "{}={}".format(name, repr(object)) for name, object in kwargs.items()
        )
        raise UnknownFunctionError(
            "{}({})".format(function.__qualname__, ", ".join(arguments))
        )

    return _unknown


class UnknownFunctionError(Exception):
    pass


class PangoRenderer(m_base_renderer.BaseRenderer):
    def __init__(self, transitions=None, font_size=11):
        super().__init__()

        self.transitions = transitions
        self.font_size = font_size

        self._transitions = _TRANSITIONS if transitions is None else transitions
        self._heading_sizes = {
            level: round(font_size * scale)
            for level, scale in zip(range(1, 7), [2, 1.5, 1.17, 1, 0.83, 0.67])
        }

        self._indents = []
        self._number = None
        self._number_template = None

    def render_document(self, document):
        yield from self._render(document)

    def render_paragraph(self, paragraph):
        yield from self._render(paragraph)

    def render_raw_text(self, raw_text):
        yield GLib.markup_escape_text(raw_text.content)

    def render_line_break(self, line_break):
        if line_break.soft:
            yield " "

        else:
            yield from self.newline(None, None)

    def render_heading(self, heading):
        yield "<span font='Bold {}'{}>".format(
            self._heading_sizes[heading.level],
            " underline='low'" if heading.level <= 2 else "",
        )
        yield from self._render(heading)
        yield "</span>"

    def render_emphasis(self, emphasis):
        yield "<i>"
        yield from self._render(emphasis)
        yield "</i>"

    def render_strong(self, strong):
        yield "<b>"
        yield from self._render(strong)
        yield "</b>"

    def render_strikethrough(self, strikethrough):
        yield "<s>"
        yield from self._render(strikethrough)
        yield "</s>"

    def render_inline_code(self, inline_code):
        yield "<span font='mono' bgcolor='#eff1f3'>"
        yield from self._render(inline_code)
        yield "</span>"

    def render_block_code(self, block_code):
        yield "<span font='mono' bgcolor='#eff1f3'>"
        yield from self._render(block_code)
        yield "</span>"

    def render_list(self, list):
        parent_number = self._number
        parent_number_template = self._number_template

        if list.start is None:
            self._number = None

        else:
            self._number = list.start
            self._number_template = "{{:{}}}".format(
                int(math.log(list.start + len(list.children) - 1, 10)) + 1
            )

        yield from self._render(list)

        self._number = parent_number
        self._number_template = parent_number_template

    def render_list_item(self, list_item):
        if self._number is None:
            _ = " • "

        else:
            _ = " {}. ".format(self._number_template.format(self._number))
            self._number += 1

        yield _
        self._indents.append(["<span alpha='1'>{}</span>".format(_)])
        yield from self._render(list_item)
        del self._indents[-1]

    def render_link(self, link):
        yield "<span color='#0969DA'>"
        yield from self._render(link)
        yield " >>> "
        yield link.target
        yield "</span>"

    def render_quote(self, quote):
        _ = " <span color='#d0d7de'>|</span> "
        yield "<span color='#57606a'>"
        yield _
        self._indents.append([_])
        yield from self._render(quote)
        del self._indents[-1]
        yield "</span>"

    def render_thematic_break(self, thematic_break):
        yield "=" * 80

    def _render(self, object):
        previous_child = None

        for child in object.children:
            transition = self._transitions.get(
                (type(previous_child), type(child)), None
            )
            if transition is not None:
                yield from transition(self, previous_child, child)

            yield from self.render(child)
            previous_child = child

    def _indent(self):
        for indent in self._indents:
            yield from indent

    def newline(self, before, after):
        yield "\n"
        yield from self._indent()

    def two_newlines(self, before, after):
        yield from self.newline(before, None)
        yield from self.newline(None, after)

    @unknown
    def render_image():
        pass

    @unknown
    def render_auto_link():
        pass

    @unknown
    def render_escape_sequence():
        pass

    @unknown
    def render_setext_heading():
        pass

    @unknown
    def render_code_fence():
        pass

    @unknown
    def render_table():
        pass

    @unknown
    def render_table_row():
        pass

    @unknown
    def render_table_cell():
        pass


_P = mistletoe.block_token.Paragraph
_H = mistletoe.block_token.Heading
_L = mistletoe.block_token.List
_LI = mistletoe.block_token.ListItem
_CF = mistletoe.block_token.CodeFence
_TB = mistletoe.block_token.ThematicBreak
_Q = mistletoe.block_token.Quote
_BC = mistletoe.block_token.BlockCode

_transitions = {1: PangoRenderer.newline, 2: PangoRenderer.two_newlines}

# fmt: off
_transition_matrix = [
    [     _P, _H, _L, _LI, _CF, _TB, _Q, _BC],
    [_P ,  2,  2,  2,   0,   2,   2,  2,   2],
    [_H ,  2,  2,  2,   0,   2,   2,  2,   2],
    [_L ,  2,  2,  2,   0,   2,   2,  2,   2],
    [_LI,  0,  0,  0,   2,   0,   0,  0,   0],
    [_CF,  2,  2,  2,   0,   2,   2,  2,   2],
    [_TB,  2,  2,  2,   0,   2,   2,  2,   2],
    [_Q,   2,  2,  2,   0,   2,   2,  2,   2],
    [_BC,  1,  1,  1,   0,   1,   1,  1,   1],
]
# fmt: on

_TRANSITIONS = {
    (row[0], after): _transitions[transition]
    for row in _transition_matrix[1:]
    for after, transition in zip(_transition_matrix[0], row[1:])
    if transition != 0
}
