import mt_pango
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
import mistletoe
import pytest


_renderer = mt_pango.PangoRenderer()


@pytest.fixture()
def show(pytestconfig):
    return pytestconfig.getoption("show")


def test_some_1(show):
    _ = """
a

b  
c


# H1
## H2
### H3
#### H4
##### H5
###### H6


*asterisks*  
**asterisks**  
**asterisks and _underscores_**  
~~Scratch this.~~


1. First ordered list item
2. Another item
    * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
    1. Ordered sub-list
4. And another item.

   You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

   To have a line break without a paragraph, you will need to use two trailing spaces.  
   Note that this line is separate, but within the same paragraph.
   (This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses


`code`

```javascript
var s = "JavaScript syntax highlighting";

alert(s);
```
""".strip()
    _test(_, show)


def test_some_2(show):
    import requests

    _ = requests.get(
        "https://raw.githubusercontent.com/mxstbr/markdown-test-file/master/TEST.md"
    ).text

    _test(_, show)


def _test(string, show):
    result = "".join(_renderer.render(mistletoe.Document(string)))
    label = Gtk.Label()
    label.set_markup(result)
    if show:
        _show(label)
    assert label.get_text() != ""


def _show(label):
    window = Gtk.Window()
    window.show()
    window.connect("destroy", Gtk.main_quit)

    scrolled_window = Gtk.ScrolledWindow()
    scrolled_window.show()
    window.add(scrolled_window)

    label.show()
    label.set_line_wrap(True)
    label.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1))
    scrolled_window.add(label)

    Gtk.main()
